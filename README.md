# README #

### Project ###
Console utility to count active user sessions. Reads csv-like log file into 
RAM and processes unique user session counts within a target day. 

#### Features ####
1. Supports the single query mode. In this mode the code will process only necessary 
part of the file.
2. Supports the interactive mode that allows multiple fast searches across cached into RAM
data.

#### Samples ####
1. Single query mode:
```
java -jar active_cookie.jar -f "test2.log" -d 2018-04-01
```

2. Interactive mode
```
java -jar active_cookie.jar -f "test2.log" -i
```

### Optimization ideas ###
1. Store original timestamp (in UTC) in the log file. This feature allows avoiding date 
time parsing, which is quite slow.

2. In case of very big logs, we could probably write them in some chunks to the FS, 
and then we could run something like map-reduce operation. Mappers will run 
in independent threads or may be in some process workers on different machines. 
Reducers will combine result from and count the most frequent session.

3. Use binary (ex: protobuf) files to speed up the parsing.

4. Use sharding by cookie UUID. So, we can split all incoming requests into some sharded 
logs where all traffic will be split equally across all available cluster machines. 
This allows increasing of parallelization of top frequent cookie computation.

5. Use tree like structure to store timestamps with cookies.  