package com.texnedo;

import com.texnedo.parser.CsvParser;
import com.texnedo.processor.SessionCounter;
import org.apache.commons.cli.*;
import org.apache.log4j.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.texnedo.utils.Utils.isEmpty;

public class Main {
    private static final String[] headers = {"cookie", "timestamp"};
    private static final AtomicBoolean shouldExit = new AtomicBoolean(false);
    protected static final Logger logger = LogManager.getLogger("active_cookie");
    private static final String LOG_FILE = "active_cookie.log";
    static {
        configureLogger();
    }
    /**
     * cookie,timestamp
     * AtY0laUfhglK3lC7,2018-12-09T14:19:00+00:00
     * SAZuXPGUrfbcn5UA,2018-12-09T10:13:00+00:00
     * 5UAVanZf6UtGyKVS,2018-12-09T07:25:00+00:00
     * AtY0laUfhglK3lC7,2018-12-09T06:19:00+00:00
     * SAZuXPGUrfbcn5UA,2018-12-08T22:03:00+00:00
     * 4sMM2LxV07bPJzwf,2018-12-08T21:30:00+00:00
     * fbcn5UAVanZf6UtG,2018-12-08T09:30:00+00:00
     * 4sMM2LxV07bPJzwf,2018-12-07T23:30:00+00:00
     *
     * Process the log file and return the most active cookie for a specific day.
     * */
    public static void main(String[] args) throws IOException, ParseException {
        final Options options = buildOptions();
        final CommandLineParser cmdParser = new DefaultParser();
        final HelpFormatter formatter = new HelpFormatter();

        try {
            final CommandLine cmd = cmdParser.parse(options, args);
            final String path = cmd.getOptionValue("file");
            final CsvParser parser = new CsvParser(logger, path, headers);
            final SessionCounter counter = new SessionCounter(logger);
            if (!cmd.hasOption("interactive")) {
                final String date = cmd.getOptionValue("date");
                if (isEmpty(date)) {
                    System.out.println("Option 'date' must be provided in a not interactive mode");
                    logger.error("Empty 'date' received, exiting");
                    System.exit(1);
                }
                counter.setPrefetchDate(date);
                runFileParsing(parser, counter);
                runSessionCounting(counter, date);
            } else {
                System.out.println("Running in the interactive move");
                Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                    System.out.println("Exiting from interactive mode");
                    logger.debug("Exiting from interactive mode");
                    shouldExit.set(true);
                }));
                final BufferedReader reader = new BufferedReader(
                        new InputStreamReader(System.in));
                runFileParsing(parser, counter);
                while (!shouldExit.get()) {
                    System.out.println("Enter date to count sessions (format: '2018-12-07')");
                    final String date = reader.readLine();
                    logger.debug("Received date to query: " + date);
                    if (isEmpty(date)) {
                        System.out.println("Option 'date' must be not empty");
                        logger.debug("Empty 'date' received, go to the next step");
                        continue;
                    }
                    runSessionCounting(counter, date);
                }
            }
        } catch (org.apache.commons.cli.ParseException ex) {
            System.out.println("Failed to parse provided arguments (see logs for the details)");
            formatter.printHelp("utility-name", options);
            logger.error("Failed to parse provided arguments", ex);
            System.exit(1);
        }
    }

    private static void runSessionCounting(final SessionCounter counter, final String date) {
        logger.debug("Start session counting");
        final long start = System.nanoTime();
        System.out.println(counter.getMostFrequentCookies(date));
        logger.debug(String.format("End session counting, elapsed time = %d", (System.nanoTime() - start) / 1000000));
    }

    private static void runFileParsing(final CsvParser parser, final SessionCounter counter)
            throws IOException, ParseException {
        logger.debug("Start parsing");
        final long start = System.nanoTime();
        parser.parseFile(counter);
        logger.debug(String.format("End parsing, elapsed time = %d", (System.nanoTime() - start) / 1000000));
    }

    private static Options buildOptions() {
        final Options options = new Options();

        final Option fileOption = new Option("f", "file", true, "csv log file path");
        fileOption.setRequired(true);
        options.addOption(fileOption);

        final Option dateOption = new Option("d", "date", true, "date to count sessions (format: '2018-12-07')");
        dateOption.setRequired(false);
        options.addOption(dateOption);

        final Option interactiveOption = new Option("i", "interactive", false, "interactive mode (for multiple requests)");
        interactiveOption.setRequired(false);
        options.addOption(interactiveOption);

        return options;
    }

    private static void configureLogger() {
        final ConsoleAppender console = new ConsoleAppender();
        final String PATTERN = "%d [%p|%c|%C{1}] %m%n";
        console.setLayout(new PatternLayout(PATTERN));
        console.setThreshold(Level.FATAL);
        console.activateOptions();
        logger.addAppender(console);

        final FileAppender file = new FileAppender();
        file.setName("FileLogger");
        file.setFile(LOG_FILE);
        file.setLayout(new PatternLayout("%d %-5p [%c{1}] %m%n"));
        file.setThreshold(Level.ALL);
        file.setAppend(true);
        file.activateOptions();
        logger.addAppender(file);
    }
}
