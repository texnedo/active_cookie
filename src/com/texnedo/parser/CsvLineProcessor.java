package com.texnedo.parser;

import java.text.ParseException;

public interface CsvLineProcessor {
    void initialize(final int estimatedLineCount);

    boolean shouldBeSkipped(final String line);

    boolean process(final String[] headers, final String[] values)
                        throws IllegalArgumentException, ParseException;
}
