package com.texnedo.parser;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;

import static com.texnedo.utils.Utils.estimateLineCount;
import static com.texnedo.utils.Utils.isEmpty;

public final class CsvParser {
    private static final String CSV_FILE_DELIMITER = ",";
    private final Logger logger;
    private final String path;
    private final String[] expectedHeaders;

    public CsvParser(final Logger logger, final String path, final String[] expectedHeaders) {
        this.logger = logger;
        this.path = path;
        this.expectedHeaders = expectedHeaders;
    }

    public void parseFile(final CsvLineProcessor processor) throws IOException, ParseException {
        logger.debug(
                String.format(Locale.US, "Start parsing file %s with target headers %s",
                    path, Arrays.toString(expectedHeaders)));
        int[] headerIndexes = null;
        boolean initialized = false;
        int errorCounter = 0;
        int lineCounter = 0;
        int skippedCounter = 0;
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (headerIndexes == null) {
                    headerIndexes = parseHeaders(line);
                } else {
                    if (!initialized) {
                        processor.initialize(estimateLineCount(path, line));
                        initialized = true;
                    }
                    if (processor.shouldBeSkipped(line)) {
                        skippedCounter++;
                        continue;
                    }
                    final String[] args = line.split(CSV_FILE_DELIMITER);
                    if (args.length < expectedHeaders.length) {
                        throw new IllegalArgumentException();
                    }
                    final String[] parsedLine = new String[expectedHeaders.length];
                    for (int i = 0; i < expectedHeaders.length; i++) {
                        parsedLine[i] = args[headerIndexes[i]];
                    }
                    if (!processor.process(expectedHeaders, parsedLine)) {
                        logger.error("Failed to parse line: " + line);
                        errorCounter++;
                    }
                    lineCounter++;
                }
            }
        }
        logger.debug(String.format(Locale.US, "File parsing completed (%d errors, %d lines, %d skipped)",
                errorCounter, lineCounter, skippedCounter));
    }

    private int[] parseHeaders(final String line)
            throws IllegalArgumentException {
        if (isEmpty(line)) {
            throw new IllegalArgumentException("Header line must be not empty");
        }
        if (expectedHeaders == null || expectedHeaders.length == 0) {
            throw new IllegalArgumentException("Expected headers must be provided");
        }
        final HashMap<String, Integer> expectedHeadersSet = new HashMap<>();
        for (int i = 0; i < expectedHeaders.length; i++) {
            if (isEmpty(expectedHeaders[i])) {
                throw new IllegalArgumentException("Empty expected header provided");
            }
            expectedHeadersSet.put(expectedHeaders[i], i);
        }
        final String[] args = line.split(CSV_FILE_DELIMITER);
        if (args.length < expectedHeaders.length) {
            throw new IllegalArgumentException("Too few headers found in file");
        }
        logger.debug("Found headers: " + line);
        final int[] headerIndexes = new int[expectedHeaders.length];
        int countFoundIndexes = 0;
        for (int i = 0; i < args.length; i++) {
            final Integer index = expectedHeadersSet.get(args[i]);
            if (index != null) {
                headerIndexes[index] = i;
                countFoundIndexes++;
            }
        }
        if (countFoundIndexes != expectedHeaders.length) {
            throw new IllegalArgumentException("Not all required headers were found");
        }
        logger.debug("Required headers indexes: " + Arrays.toString(headerIndexes));
        return headerIndexes;
    }
}
