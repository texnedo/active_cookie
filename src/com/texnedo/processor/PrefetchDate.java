package com.texnedo.processor;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import static java.time.format.DateTimeFormatter.ISO_DATE;

final class PrefetchDate {
    private static final DateTimeFormatter prefetchDateFormat =
            DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.US);

    final String targetDate;
    final String targetDateStart;
    final String targetDateEnd;

    PrefetchDate(final String date) {
        targetDate = date;
        final LocalDate target =
                LocalDate.parse(targetDate, ISO_DATE);
        final LocalDate start = target.minusDays(1);
        final LocalDate end = target.plusDays(1);
        targetDateStart = prefetchDateFormat.format(start);
        targetDateEnd = prefetchDateFormat.format(end);
    }
}
