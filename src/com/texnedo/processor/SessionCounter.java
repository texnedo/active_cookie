package com.texnedo.processor;

import com.texnedo.parser.CsvLineProcessor;
import org.apache.log4j.Logger;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static com.texnedo.utils.Utils.isEmpty;
import static java.time.format.DateTimeFormatter.ISO_DATE;

public class SessionCounter implements CsvLineProcessor {
    private static final DateFormat dateFormat =
            new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX", Locale.US);
    private final Logger logger;
    private SessionData data;
    private PrefetchDate prefetch;

    public SessionCounter(final Logger logger) {
        this.logger = logger;
    }

    @Override
    public void initialize(final int estimatedLineCount) {
        logger.debug("Initialize memory for estimated lines count: " + estimatedLineCount);
        data = new SessionData(estimatedLineCount);
    }

    @Override
    public boolean shouldBeSkipped(final String line) {
        if (isEmpty(line)) {
            return true;
        }
        if (prefetch == null) {
            return false;
        }
        //TODO: support stopping line processing in case we have already read all the data according
        // to the provided date
        if (line.contains(prefetch.targetDate) ||
                line.contains(prefetch.targetDateStart) ||
                line.contains(prefetch.targetDateEnd)) {
            return false;
        }
        return true;
    }

    @Override
    public boolean process(final String[] headers, final String[] values) throws IllegalArgumentException, ParseException {
        if (data == null) {
            throw new IllegalStateException("initialize() must be called first");
        }
        final String cookie = values[0];
        final String timestamp = values[1];
        data.cookies.add(cookie);
        data.timestamps.add(dateFormat.parse(timestamp).getTime() / 1000);
        return true;
    }

    public void setPrefetchDate(final String targetDate) {
        prefetch = new PrefetchDate(targetDate);
    }

    public Set<String> getMostFrequentCookies(final String targetDate) {
        logger.debug("Start counting for date: " + targetDate);
        final long targetStartTimestamp =
                LocalDate.parse(targetDate, ISO_DATE)
                        .toEpochSecond(LocalTime.of(0, 0), ZoneOffset.UTC);
        final long targetEndTimestamp = targetStartTimestamp + TimeUnit.DAYS.toSeconds(1);
        int result = Collections.binarySearch(data.timestamps, targetStartTimestamp, Comparator.reverseOrder());
        if (result < 0) {
            result = -result - 1;
            //Move back one record as binary search will return the first element that lower than start
            //of the period
            result--;
        }
        final HashMap<String, Long> sessionCounts = new HashMap<>();
        int index = result;
        long maxCookieCount = -1;
        final HashSet<String> maxCookies = new HashSet<>();
        while (index >= 0 && data.timestamps.get(index) <= targetEndTimestamp) {
            final String cookie = data.cookies.get(index);
            Long count = sessionCounts.get(cookie);
            if (count == null) {
                count = 1L;
            } else {
                count++;
            }
            sessionCounts.put(cookie, count);
            if (count == maxCookieCount) {
                maxCookies.add(cookie);
            } else if (count > maxCookieCount) {
                maxCookies.clear();
                maxCookies.add(cookie);
                maxCookieCount = count;
            }
            index--;
        }
        logger.debug("Unique session count: " + sessionCounts.size());
        logger.debug("Found most frequent session count: " + maxCookieCount);
        logger.debug("Found most frequent sessions: " + maxCookies);
        return maxCookies;
    }
}
