package com.texnedo.processor;

import java.util.ArrayList;

final class SessionData {
    final ArrayList<String> cookies;
    final ArrayList<Long> timestamps;

    SessionData() {
        timestamps = new ArrayList<>();
        cookies = new ArrayList<>();
    }

    SessionData(final int estimatedLines) {
        if (estimatedLines < 1000) {
            timestamps = new ArrayList<>();
            cookies = new ArrayList<>();
        } else {
            timestamps = new ArrayList<>(estimatedLines);
            cookies = new ArrayList<>(estimatedLines);
        }
    }
}
