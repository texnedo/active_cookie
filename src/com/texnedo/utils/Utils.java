package com.texnedo.utils;

import java.io.File;
import java.nio.charset.StandardCharsets;

public class Utils {
    public static boolean isEmpty(final String text) {
        return text == null || text.length() == 0;
    }

    public static int estimateLineCount(final String path, final String line) {
        final File file = new File(path);
        long fileSize = file.length();
        final byte[] data = line.getBytes(StandardCharsets.UTF_8);
        return (int) fileSize / data.length;
    }
}
