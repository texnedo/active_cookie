package com.texnedo;

import com.texnedo.parser.CsvParser;
import com.texnedo.processor.SessionCounter;
import org.apache.log4j.LogManager;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.text.ParseException;
import java.time.OffsetDateTime;
import java.util.*;

import static com.texnedo.parser.CsvParserTest.testFileHeaders;
import static java.time.format.DateTimeFormatter.ISO_OFFSET_DATE_TIME;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MainTest {
    private static final String TESTS_LOGS_BIG_PATH = "tests_logs/test2_big.csv";

    @Test
    public void testReadMain() throws IOException, ParseException {
        try {
            generateTestCsvFile(TESTS_LOGS_BIG_PATH, 100, 10000000);
            final SessionCounter counter = new SessionCounter(LogManager.getLogger("test"));
            final CsvParser parser = new CsvParser(
                    LogManager.getLogger("test"),
                    TESTS_LOGS_BIG_PATH,
                    testFileHeaders
            );
            parser.parseFile(counter);
            final Set<String> result1_prod = counter.getMostFrequentCookies("2018-12-09");
            Set<String> result1_test = readTestCsvFileAndCount(TESTS_LOGS_BIG_PATH, "2018-12-09");
            assertEquals(result1_prod, result1_test);

            final Set<String> result2_prod = counter.getMostFrequentCookies("2018-12-07");
            Set<String> result2_test = readTestCsvFileAndCount(TESTS_LOGS_BIG_PATH, "2018-12-07");
            assertEquals(result2_prod, result2_test);
        } finally {
            final File file = new File(TESTS_LOGS_BIG_PATH);
            file.delete();
        }
    }

    @Test
    public void testPerformance() throws IOException, ParseException {
        try {
            generateTestCsvFile(TESTS_LOGS_BIG_PATH, 10, 1000000);
            final SessionCounter counter = new SessionCounter(LogManager.getLogger("test"));
            final CsvParser parser = new CsvParser(
                    LogManager.getLogger("test"),
                    TESTS_LOGS_BIG_PATH,
                    testFileHeaders
            );
            long start = System.nanoTime();
            parser.parseFile(counter);
            final Set<String> result1 = counter.getMostFrequentCookies("2018-12-07");
            long diff1 = (System.nanoTime() - start) / 1000000;
            System.out.println(diff1);
            System.out.println(result1);

            long start1 = System.nanoTime();
            final Set<String> result2 = counter.getMostFrequentCookies("2018-12-09");
            long diff2 = (System.nanoTime() - start1) / 1000000;
            System.out.println(diff2);
            System.out.println(result2);
            assertTrue(diff2 * 100 < diff1);

            final SessionCounter counter2 = new SessionCounter(LogManager.getLogger("test"));
            counter2.setPrefetchDate("2018-12-07");
            long start3 = System.nanoTime();
            parser.parseFile(counter2);
            final Set<String> result3 = counter.getMostFrequentCookies("2018-12-07");
            long diff3 = (System.nanoTime() - start3) / 1000000;
            System.out.println(diff3);
            System.out.println(result3);
            assertTrue(diff3 * 5 < diff1);
            assertEquals(result1, result3);
        } finally {
            final File file = new File(TESTS_LOGS_BIG_PATH);
            file.delete();
        }
    }

    private Set<String> readTestCsvFileAndCount(final String path, final String date) throws IOException {
        final HashMap<String, Long> sessionCounts = new HashMap<>();
        System.out.println("Start parsing");
        long start = System.nanoTime();
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (line.contains(date)) {
                    final String cookie = line.split(",")[0];
                    Long count = sessionCounts.get(cookie);
                    if (count == null) {
                        count = 1L;
                    } else {
                        count++;
                    }
                    sessionCounts.put(cookie, count);
                }
            }
        }
        System.out.println(String.format("End parsing, elapsed time = %d", (System.nanoTime() - start) / 1000000));
        final PriorityQueue<Map.Entry<String, Long>> topSessions = new PriorityQueue<>(new Comparator<Map.Entry<String, Long>>() {
            @Override
            public int compare(Map.Entry<String, Long> o1, Map.Entry<String, Long> o2) {
                return Long.compare(o2.getValue(), o1.getValue());
            }
        });
        topSessions.addAll(sessionCounts.entrySet());
        Set<String> result = new HashSet<>();
        if (!topSessions.isEmpty()) {
            final long topCount = topSessions.peek().getValue();
            while (!topSessions.isEmpty()) {
                final Map.Entry<String, Long> session = topSessions.poll();
                if (session.getValue() != topCount) {
                    break;
                }
                result.add(session.getKey());
                System.out.println(String.format(Locale.US, "%s - %d", session.getKey(), session.getValue()));
            }
        }
        return result;
    }

    private static void generateTestCsvFile(final String path, int userCount, int lineCount) throws IOException {
        final File file = new File(path);
        file.delete();
        final String[] cookies = new String[userCount];
        for (int i = 0; i < cookies.length; i++) {
            cookies[i] = UUID.randomUUID().toString();
        }
        final Random rnd = new Random(0);
        final OffsetDateTime start = OffsetDateTime.parse(
                "2018-12-09T14:19:00+00:00",
                ISO_OFFSET_DATE_TIME
        );
        OffsetDateTime last = start;
        try (BufferedWriter br = new BufferedWriter(new FileWriter(path))) {
            br.write("cookie,timestamp");
            br.newLine();
            for (int i = 0; i < lineCount; i++) {
                final int index = rnd.nextInt(cookies.length);
                final String cookie = cookies[index];
                br.write(String.format(Locale.US, "%s,%s", cookie, ISO_OFFSET_DATE_TIME.format(last)));
                br.newLine();
                last = last.minusMinutes(rnd.nextInt(30));
            }
        }
    }
}