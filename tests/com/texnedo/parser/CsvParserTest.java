package com.texnedo.parser;

import org.apache.log4j.LogManager;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class CsvParserTest {
    private static final class TestProcessor implements CsvLineProcessor {
        boolean isInitialized;
        int estimatedLineCount;
        boolean skipLines = false;
        ArrayList<String[]> allHeaders = new ArrayList<>();
        ArrayList<String[]> allValues = new ArrayList<>();
        ArrayList<String> skippedLines = new ArrayList<>();

        @Override
        public void initialize(int count) {
            isInitialized = true;
            estimatedLineCount = count;
        }

        @Override
        public boolean shouldBeSkipped(String line) {
            skippedLines.add(line);
            return skipLines;
        }

        @Override
        public boolean process(String[] headers, String[] values) throws IllegalArgumentException, ParseException {
            allHeaders.add(headers);
            allValues.add(values);
            return true;
        }
    }

    public static final String[][] testFileLines = new String[][]
            {{"AtY0laUfhglK3lC7","2018-12-09T14:19:00+05:00"},
            {"AtY0laUfhglK3lC7","2018-12-09T14:19:00+01:00"},
            {"SAZuXPGUrfbcn5UA","2018-12-09T10:13:00+00:00"},
            {"5UAVanZf6UtGyKVS","2018-12-09T07:25:00+00:00"},
            {"AtY0laUfhglK3lC7","2018-12-09T06:19:00+00:00"},
            {"SAZuXPGUrfbcn5UA","2018-12-08T22:03:00+00:00"},
            {"4sMM2LxV07bPJzwf","2018-12-08T21:30:00+00:00"},
            {"fbcn5UAVanZf6UtG","2018-12-08T09:30:00+00:00"},
            {"4sMM2LxV07bPJzwf","2018-12-07T23:30:00+00:00"}};

    public static final String[] testFileHeaders = {"cookie","timestamp"};

    @Test
    public void testParseFile() throws IOException, ParseException {
        final CsvParser parser = new CsvParser(
                LogManager.getLogger("test"),
                "tests_logs/test1.csv",
                testFileHeaders
                );
        final TestProcessor processor = new TestProcessor();
        parser.parseFile(processor);
        assertEquals(processor.estimatedLineCount, 9);
        assertTrue(processor.isInitialized);
        assertFalse(processor.allHeaders.isEmpty());
        assertFalse(processor.allValues.isEmpty());
        for (int i = 0; i < processor.allValues.size(); i++) {
            assertArrayEquals(processor.allValues.get(i), testFileLines[i]);
            assertArrayEquals(processor.allHeaders.get(i), testFileHeaders);
        }
    }

    @Test()
    public void testParseFileWrongColumns() throws IOException, ParseException {
        assertThrows(IllegalArgumentException.class, () -> {
            final CsvParser parser = new CsvParser(
                    LogManager.getLogger("test"),
                    "tests_logs/test1.csv",
                    new String[] {"cookie1","timestamp1"}
            );
            final TestProcessor processor = new TestProcessor();
            parser.parseFile(processor);
        });
    }

    @Test
    public void testSkipLines() throws IOException, ParseException {
        final CsvParser parser = new CsvParser(
                LogManager.getLogger("test"),
                "tests_logs/test1.csv",
                testFileHeaders
        );
        final TestProcessor processor = new TestProcessor();
        processor.skipLines = true;
        parser.parseFile(processor);
        assertTrue(processor.allHeaders.isEmpty());
        assertTrue(processor.allValues.isEmpty());
        assertEquals(processor.skippedLines.size(), 9);
    }
}