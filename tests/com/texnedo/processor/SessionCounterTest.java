package com.texnedo.processor;

import com.texnedo.parser.CsvParser;
import org.apache.log4j.LogManager;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.text.ParseException;
import java.util.Set;

import static com.texnedo.parser.CsvParserTest.testFileHeaders;
import static org.junit.jupiter.api.Assertions.*;

class SessionCounterTest {
    @Test
    public void testSessionCounterBasic() throws IOException, ParseException {
        final SessionCounter counter = new SessionCounter(LogManager.getLogger("test"));
        final CsvParser parser = new CsvParser(
                LogManager.getLogger("test"),
                "tests_logs/test1.csv",
                testFileHeaders
        );
        parser.parseFile(counter);
        final Set<String> result1 = counter.getMostFrequentCookies("2018-12-09");
        assertEquals(result1.size(), 1);
        assertTrue(result1.contains("AtY0laUfhglK3lC7"));

        final Set<String> result2 = counter.getMostFrequentCookies("2018-12-08");
        assertEquals(result2.size(), 3);
        assertTrue(result2.contains("SAZuXPGUrfbcn5UA"));
        assertTrue(result2.contains("4sMM2LxV07bPJzwf"));
        assertTrue(result2.contains("fbcn5UAVanZf6UtG"));

        final Set<String> result3 = counter.getMostFrequentCookies("2018-12-07");
        assertEquals(result3.size(), 1);
        assertTrue(result3.contains("4sMM2LxV07bPJzwf"));

        final Set<String> result4 = counter.getMostFrequentCookies("2018-12-01");
        assertEquals(result4.size(), 0);

        final Set<String> result5 = counter.getMostFrequentCookies("2019-12-01");
        assertEquals(result5.size(), 0);
    }

    @Test
    public void testSessionCounterPrefetch() {
        final SessionCounter counter = new SessionCounter(LogManager.getLogger("test"));
        counter.setPrefetchDate("2018-12-07");
        assertFalse(counter.shouldBeSkipped("AtY0laUfhglK3lC7,2018-12-07T14:19:00+05:00"));
        assertFalse(counter.shouldBeSkipped("AtY0laUfhglK3lC7,2018-12-06T14:19:00+05:00"));
        assertFalse(counter.shouldBeSkipped("AtY0laUfhglK3lC7,2018-12-08T14:19:00+05:00"));
        assertTrue(counter.shouldBeSkipped("AtY0laUfhglK3lC7,2018-12-05T14:19:00+05:00"));
        assertTrue(counter.shouldBeSkipped("AtY0laUfhglK3lC7,2018-12-10T14:19:00+05:00"));
    }
}